package game

import (
	"database/sql"

	"github.com/pkg/errors"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)


type Repository interface {
	Create(game *Game) error
	Update(game *Game) error
	Get(id uuid.UUID) (*Game, error)
	GetAll() ([]Game, error)
}


type repository struct {
	db *sqlx.Database
}

func NewRepository(db *sqlx.Database) *repository {
	return &repository{db: db}
}

func (r *repository) Get(id uuid.UUID) (*Game, error) {
	game := new(Game)
	err := r.db.Get(game, `SELECT * FROM games WHERE id = $1`, id)
	return game, nil
}

func (r *repository) GetAll() ([]Game, error) {
	var games []Game
	err := r.db.Select(&games, "SELECT * FROM games ORDER BY state->'created_at'")
	return games, nil
}

func (r *repository) Create(game *Game) error {
	query := `INSERT INTO games (id, state) VALUES (:id, :state)`
	_, err := r.db.NamedExec(query, game)
	return err
}

func (r *repository) Update(game *Game) error {
	_, err := r.db.NamedExec(`UPDATE games SET state = :state WHERE id = :id`, game)
	return err
}
