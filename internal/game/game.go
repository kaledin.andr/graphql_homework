package game

import (
	"time"
	"errors"
	"database/sql/driver"
	"encoding/json"

	"github.com/google/uuid"
)

const (
	RoleCitizen Role = iota + 1
	RoleMafia
)

type Game struct {
	ID uuid.UUID `db:"id"`
	State State `db:"state"`
}

type Comment struct {
	ID   uuid.UUID `json:"id"`
	Text string    `json:"text"`
}

type Scoreboard struct {
	Players []Player `json:"users"`
}

type Role uint

type State struct {
	CreatedAt  time.Time  `json:"created_at"`
	IsFinished bool       `json:"is_finished"`
	Comments   []Comment  `json:"comments"`
	Scoreboard Scoreboard `json:"scoreboard"`
}

type Player struct {
	Nickname 	string `json:"nickname"`
	KillCounter int    `json:"killCounter"`
	Role		Role   `json:"role"`
	IsAlive		bool   `json:"is_alive"`
}

func (s *State) Value() (driver.Value, error) {
	return json.Marshal(*s)
}

func (s *State) Scan(value interface{}) error {
	b, ok := value.([]byte)
	return json.Unmarshal(b, s)
}
