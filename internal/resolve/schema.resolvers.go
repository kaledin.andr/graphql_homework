package resolve

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/kaledin.andr/graphql_homework/internal/gqlgenerated"
	"gitlab.com/kaledin.andr/graphql_homework/internal/gqlmodel"
	"gitlab.com/kaledin.andr/graphql_homework/internal/game"
	"github.com/pkg/errors"
	zlog "github.com/rs/zerolog/log"
)

func (r *mutationResolver) CreateComment(ctx context.Context, gameID string, text string) (*gqlmodel.Comment, error) {
	parsed, err := uuid.Parse(gameID)
	if err != nil {
		return nil, errors.Wrap(err, "invalid game id UUID format")
	}

	m, err := r.repository.Get(parsed)
	if err != nil {
		if !errors.Is(err, game.ErrNotFound) {
			zlog.Err(err).Str("id", gameID).Msg("failed to get game to comment")
		}

		return nil, err
	}

	c := game.Comment{
		ID:   uuid.New(),
		Text: text,
	}
	m.State.Comments = append(m.State.Comments, c)

	err = r.repository.Update(m)
	if err != nil {
		zlog.Err(err).Str("id", m.ID.String()).Msg("failed to update game")
		return nil, err
	}

	converted := commentToModel(c)

	return &converted, nil
}

func (r *queryResolver) Game(ctx context.Context, id string) (*gqlmodel.Game, error) {
	parsed, err := uuid.Parse(id)
	if err != nil {
		return nil, errors.Wrap(err, "invalid UUID format")
	}

	m, err := r.repository.Get(parsed)
	if err != nil {
		if !errors.Is(err, game.ErrNotFound) {
			zlog.Err(err).Str("id", id).Msg("failed to get game")
		}

		return nil, err
	}

	return gameToModel(m), err
}

func (r *queryResolver) Games(ctx context.Context, isFinished *bool, limit *int, offset *int) ([]gqlmodel.Game, error) {
	games, err := r.repository.GetAll()
	if err != nil {
		zlog.Err(err).Msg("failed to get games")
		return nil, err
	}

	var result []gqlmodel.Game
	for _, m := range games {
		if isFinished != nil && *isFinished != m.State.IsFinished {
			continue
		}

		if offset != nil && *offset != 0 {
			*offset--
			continue
		}

		if limit != nil && *limit < len(result)+1 {
			break
		}

		result = append(result, *gameToModel(&m))
	}

	return result, nil
}

// Mutation returns gqlgenerated.MutationResolver implementation.
func (r *Resolver) Mutation() gqlgenerated.MutationResolver { return &mutationResolver{r} }

// Query returns gqlgenerated.QueryResolver implementation.
func (r *Resolver) Query() gqlgenerated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
