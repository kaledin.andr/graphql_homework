package resolve

import (
	"gitlab.com/kaledin.andr/graphql_homework/internal/gqlmodel"
	"gitlab.com/kaledin.andr/graphql_homework/internal/game"
)

func gameToModel(m *game.Game) *gqlmodel.Game {
	s := m.State

	return &gqlmodel.Game{
		Scoreboard: addScoreboard(&s.Scoreboard),
		Comments:   addComments(s.Comments),

		ID:         m.ID.String(),
		CreatedAt:  s.CreatedAt.String(),
		IsFinished: s.IsFinished,
	}
}

func addRole(role game.Role) gqlmodel.Role {
	switch role {
	case game.RoleMafia:
		return gqlmodel.RoleMafia
	case game.RoleCitizen:
		return gqlmodel.RoleCitizen
	default:
		return "UNKNOWN"
	}
}

func addComments(comments []game.Comment) []gqlmodel.Comment {
	result := make([]gqlmodel.Comment, 0, len(comments))
	for _, c := range comments {
		result = append(result, addComment(c))
	}

	return result
}

func addPlayer(p *game.Player) gqlmodel.Player {
	return gqlmodel.Player{
		Nickname: 		p.Nickname,
		Role:     		addRole(p.Role),
		IsAlive:  		p.IsAlive,
		KillCounter:    p.KillCounter,
	}
}

func addComment(comment game.Comment) gqlmodel.Comment {
	return gqlmodel.Comment{
		ID:   comment.ID.String(),
		Text: comment.Text,
	}
}

func addScoreboard(s *game.Scoreboard) *gqlmodel.Scoreboard {
	scoreboard := &gqlmodel.Scoreboard{Players: make([]gqlmodel.Player, 0, len(s.Players))}
	for _, p := range s.Players {
		scoreboard.Players = append(scoreboard.Players, addPlayer(&p))
	}
	return scoreboard
}
