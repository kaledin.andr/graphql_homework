package resolve

import "gitlab.com/kaledin.andr/graphql_homework/internal/game"

type Resolver struct {
	repository game.Repository
}

func NewResolver(r game.Repository) *Resolver {
	return &Resolver{repository: r}
}
