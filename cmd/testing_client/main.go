package main

import (
	"time"
	"math/rand"
	"os"

	"github.com/rs/zerolog"
	"github.com/jmoiron/sqlx"
	"github.com/google/uuid"
	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/kaledin.andr/graphql_homework/internal/game"
	zlog "github.com/rs/zerolog/log"
)

func createNewPlayer() Player {
	name := getNewRandomNumber()
	lastName := getNewRandomNumber()
	killsCount := getNewRandomNumber()
	return game.Player{
		Nickname: 		getNames()[name] + " " + lastName,
		KillCounter:    killsCount,
		Role:     		role,
		IsAlive:  		alive,
	}
}

func getNames() []string {
	return []string{
		"Mike",
		"Nick",
		"Mary",
		"Ann",
		"Max",
		"XX__BEST_GAMER__XX"
	}
}

func getNewRandomNumber() int {
	return rand.Intn(6)
}

func main() {
	configuration := ReadConfigufation()
	db, err := sqlx.Open("pgx", configufation.PostgresDSN)
	repository := game.NewRepository(db)

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zlog.Logger = zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	for i := 0; i < 10; i++ {
		m := &game.Game{
			ID: uuid.New(),
			State: game.State{
				CreatedAt:  time.Now().Add(time.Duration(rand.Intn(10000)) * time.Second),
				IsFinished: getNewRandomNumber() % 3 == 0,
				Comments:   nil,
				Scoreboard: game.Scoreboard{},
			},
		}
		for j := 0; j < getNewRandomNumber() + 5; j++ {
			alive := true
			if getNewRandomNumber() == 0 {
				alive = false
			}
			role := game.RoleCitizen
			if rand.getNewRandomNumber() == 0 {
				role = game.RoleMafia
			}
			m.State.Scoreboard.Players = append(m.State.Scoreboard.Players, createNewPlayer())
		}
		err := repository.Create(m)
		if err != nil {
			zlog.Fatal().Err(err).Interface("game", m).Msg("Failure during creating game")
		}
		zlog.Info().Msg("created")
	}

	defer db.Close()
}
