package main

import (
	"os"
	"os/signal"
	"syscall"
	"context"
	"net/http"

	"gitlab.com/kaledin.andr/graphql_homework/internal/gqlgenerated"
	"gitlab.com/kaledin.andr/graphql_homework/internal/game"
	"gitlab.com/kaledin.andr/graphql_homework/internal/resolve"

	"github.com/jmoiron/sqlx"
	"github.com/99designs/gqlgen/graphql/handler"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/rs/zerolog"
	"github.com/99designs/gqlgen/graphql/playground"
	zlog "github.com/rs/zerolog/log"
)

func main() {
	configuration := ReadConfigufation()
	_, cancel := context.WithCancel(context.Background())
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	db, err := sqlx.Open("pgx", configufation.PostgresDSN)
	repository := game.NewRepository(db)
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zlog.Logger = zlog.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	http.Handle("/query", handler.NewDefaultServer(gqlgenerated.NewExecutableSchema(
		gqlgenerated.Configufation{Resolvers: resolve.NewResolver(repository)},
	)))
	go func() {err := http.ListenAndServe(configuration.ServerAddress, nil)}()
	zlog.Info().Str("address", configuration.ServerAddress).Msg("starting server...")
	<-stop
	cancel()
	defer db.Close()
}