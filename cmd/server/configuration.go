package main

import (
	"time"

	"github.com/caarlos0/env/v6"
	zlog "github.com/rs/zerolog/log"
)

type Database struct {
	PostgresDSN string `env:"Database_POSTGRES_DSN,required" 
						envDefault:"host=localhost 
									port=5432 
									user=user 
									password=password 
									dbname=graphql 
									sslmode=disable"`
}

type Configufation struct {
	ServerAddress string `env:"SERVER_ADDRESS,required" 
						  envDefault:":8080"`

	Database Database
}

func ReadConfigufation() Configufation {
	var configuration Configufation
	err := env.Parse(&configuration)
	return configuration
}
