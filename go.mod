module gitlab.com/kaledin.andr/graphql_homework

go 1.17

require (
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.26.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/vektah/gqlparser/v2 v2.4.2
	github.com/99designs/gqlgen v0.17.5
	github.com/jackc/pgx/v4 v4.16.1
	github.com/caarlos0/env/v6 v6.9.2
	github.com/google/uuid v1.3.0
)